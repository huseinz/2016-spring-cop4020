-- based on 16.8 Set of
-- Haskell - the craft of functional programming

module Set
  (
    Set       , 
    empty     , -- Set a
    sing      , -- a -> Set a 
    memSet    , -- Ord a => Set a -> a -> Bool
    union     , -- Ord a => Set a -> Set a -> Set a
    inter     , -- Ord a => Set a -> Set a -> Set a
    diff      , -- Ord a => Set a -> Set a -> Set a
    symDiff   , -- Ord a => Set a -> Set a -> Set a
    makeSet   , -- Ord a => [a] -> Set a
    mapSet    , -- Ord b => (a -> b) -> Set a -> Set b
    foldSet   , -- (a -> a -> a) -> Set a -> Set a
    filterSet , -- (a -> Bool) -> Set a -> Set a
    powerSet  , -- Set a => Set (Set a)
    card      , -- Set a -> Int
  ) where

import Data.List hiding (union)

instance Eq a => Eq (Set a) where
  (==) = eqSet
instance Ord a => Ord (Set a) where
  (<=) = leqSet
-- Display a set in set-theory notation.  --JDJ-S                               
instance Show a => Show (Set a) where
  show (Set xs) = '{' : foldr (++) "}" (intersperse "," (map show xs))
  --show (Set xs)

newtype Set a = Set [a]

empty :: Set a
empty = Set []

sing :: a -> Set a
sing x = Set [x]

memSet :: Ord a => Set a -> a -> Bool
memSet (Set [])     y = False
memSet (Set (x:xs)) y
  | x <  y    = memSet (Set xs) y
  | x == y    = True
  | otherwise = False

union :: Ord a => Set a -> Set a -> Set a
union (Set xs) (Set ys) = Set (uni xs ys)

uni :: Ord a => [a] -> [a] -> [a]
uni [] ys = ys
uni xs [] = xs
uni (x:xs) (y:ys)
  | x <  y    = x : uni xs (y:ys)
  | x == y    = x : uni xs ys
  | otherwise = y : uni (x:xs) ys

inter :: Ord a => Set a -> Set a -> Set a
inter (Set xs) (Set ys) = Set (int xs ys)

int :: Ord a => [a] -> [a] -> [a]
int [] ys = []
int xs [] = []
int (x:xs) (y:ys)
  | x <  y    = int xs (y:ys)
  | x == y    = x : int xs ys
  | otherwise = int (x:xs) ys
    
subSet :: Ord a => Set a -> Set a -> Bool
subSet (Set xs) (Set ys) = subS xs ys

subS :: Ord a => [a] -> [a] -> Bool
subS [] ys = True
subS xs [] = False
subS (x:xs) (y:ys)
  | x <  y    = False
  | x == y    = subS xs ys
  | otherwise = subS (x:xs) ys

eqSet :: Eq a => Set a -> Set a -> Bool
eqSet (Set xs) (Set ys) = (xs == ys)

leqSet :: Ord a => Set a -> Set a -> Bool
leqSet (Set xs) (Set ys) = (xs <= ys)

makeSet :: Ord a => [a] -> Set a
makeSet = Set . remDups . sort
  where
    remDups []  = []
    remDups [x] = [x]
    remDups (x:y:xs)
      | x < y     = x : remDups (y:xs)
      | otherwise = remDups (y:xs)
        
mapSet :: (Ord b) => (a -> b) -> Set a -> Set b
mapSet f (Set xs) = makeSet (map f xs)

filterSet :: (a -> Bool) -> Set a -> Set a
filterSet p (Set xs) = Set (filter p xs)

--foldSet :: (a -> a -> a) -> a -> Set a -> a
foldSet :: (a -> b -> b) -> b -> Set a -> b
foldSet f x (Set xs) = (foldr f x xs)

instance Foldable Set where
    foldr = foldSet

card :: Set a -> Int
card (Set xs) = length xs

diff :: (Ord a) => Set a -> Set a -> Set a
{- -- Interface-only implementation, O(n²):
diff s1 s2 = filterSet (\ e -> not (memSet s2 e)) s1 
-}

-- List implementation, Θ(n):
diff (Set l1) (Set l2) = Set (diff' l1 l2) where
    diff' [] _ = [] 
    diff' l1 [] = l1
    diff' l1@(x:xs) l2@(y:ys)  
        | x < y = x:diff' xs l2
        | x == y = diff' xs ys
        | x > y = diff' l1 ys
       

symDiff :: (Ord a) => Set a -> Set a -> Set a
{- -- Interface-only implementation, Θ(n):
symDiff s1 s2 = union s1 s2 `diff` inter s1 s2
-}

-- Optimal list implementation, Θ(n):
symDiff (Set l1) (Set l2) = Set (symDiff' l1 l2) where
    symDiff' l1 [] = l1
    symDiff' [] l2 = l2
    symDiff' l1@(x:xs) l2@(y:ys)
        | x < y = x:symDiff' xs l2
        | x == y = symDiff' xs ys
        | y < x = y:symDiff' l1 ys
                       

powerSet :: (Ord a) => Set a -> Set (Set a)
-- The easy solution (relies on makeSet for sorting):
-- 
{-
powerSet (Set l) = makeSet (map Set (subsequences l))
--}

-- Optimally efficient, but not optimally readable:
powerSet (Set l) = Set (empty : map Set (foldr (\x ps -> [x] : map (x:) ps ++ ps) [] l))

-- Interface-only implementation:
--
{-
powerSet s = let setToList :: Set a -> [a]
                 setToList s = foldr (:) [] s
                 elems = setToList s
                 sets = map makeSet (subsequences elems)
             in
               makeSet sets
--}


