-- Zubir Husein
module PolygonArea where

det :: (Double, Double) -> (Double, Double) -> Double
computeArea :: [(Double,Double)] -> Double
computeAreaHelper :: [(Double,Double)] -> Double

det (a,c) (b,d) = a * d - c * b

computeArea x = (det (last x) (head x) + computeAreaHelper x)/2.0

computeAreaHelper (x:xs)  
 | length xs <= 1 = det x (head xs)
 | otherwise = det x (head xs) + computeAreaHelper xs
