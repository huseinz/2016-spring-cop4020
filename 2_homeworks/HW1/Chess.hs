-- Zubir Husein
module Chess where
import Data.Char

-- See https://en.wikipedia.org/wiki/Chess for more details
-- Recall we only consider the situation where there is only a single
-- piece on the board

-- see Rules - Set up for basic definitions

type File     = Char         -- column index
                             -- valid files are 'a','b',...,'h'
type Rank     = Int          -- row index
                             -- valid ranks are 1,2,...8
type Position = (File,Rank)   

data Color =
  Black | White
  deriving (Eq,Show)

data Piece =
  King | Queen | Rook | Bishop | Knight | Pawn
  deriving (Eq,Show)

isLegalPosition :: Position -> Bool
-- implement isValidPosition

isLegalPosition (f,r) = elem f ['a'..'h'] && elem r [1..8]

isLegalMove :: Color -> Piece -> Position -> Position -> Bool
-- implement isLegalMove


--assuming the initial position is always legal
--and that pawns can move 2 positions
isLegalMove color Pawn (f1,r1) (f2,r2) 
	| not (isLegalPosition (f2,r2)) || f1 /= f2 || r1 == r2 = False
	--black pawns start at rank 7 and decrease in rank
	| color == Black = (r1 == 7 && r1 - r2 <= 2 && r1 - r2 > 0) || r1 - r2 == 1 
	--white pawns start at rank 2 and increase in rank
	| color == White = (r1 == 2 && r2 - r1 <= 2 && r2 - r1 > 0) || r2 - r1 == 1
	| otherwise = False

isLegalMove _ piece (f1,r1) (f2,r2)
	| not (isLegalPosition (f2,r2)) || (f1,r1) == (f2,r2) = False
	--King: can move one or fewer ranks and one or fewer files
	| piece == King = abs(ord f1 - ord f2) <= 1 && abs(r1 - r2) <= 1
	--Bishop: the change in rank and file must be equal
	| piece == Bishop = abs(ord f1 - ord f2) == abs(r1 - r2)
	--Rooks: can only change in either rank or file
	| piece == Rook = (abs(ord f1 - ord f2) == 0) /= (abs(r1-r2) == 0)
	--Queens: combinination of Rook & Bishop
	| piece == Queen = isLegalMove Black Bishop (f1,r1) (f2,r2) || isLegalMove Black Rook (f1,r1) (f2,r2)
	--Knight: the most annoying one
	| piece == Knight = elem (chr (ord f2), r2) (map (\(f,r) -> (chr(f + ord f1), r + r1)) 
                               [(1,2),(-1,2),(2,-1),(-2,-1),(-1,-2),(1,-2),(-2,1),(2,1)])
	| otherwise = False
