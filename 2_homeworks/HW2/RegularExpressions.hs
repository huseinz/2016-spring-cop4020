-- Zubir Husein HW2 Problem 2
module RegularExpressions where

import Prelude hiding ((<*>))

type RegExp = String -> Bool

epsilon :: RegExp
epsilon = (== "")

char :: Char ->  RegExp
char ch = (== [ch])

(|||) :: RegExp -> RegExp -> RegExp
(|||) e1 e2 =
  \s -> e1 s || e2 s

splits :: [a] -> [([a],[a])]
-- splits "fun" ~~>
-- [("","fun"),("f","un"),
--  ("fu","n"),("fun","")]
splits xs =
  map (flip splitAt xs) [0..length xs]
-- alternatively, we could also use a list comprehension like so
-- [splitAt i xs | i <- [0..length xs]]

(<*>) :: RegExp -> RegExp -> RegExp
(<*>) e1 e2 =
  \s -> or [ e1 prefix && e2 suffix | (prefix,suffix) <- splits s]

(<**>) :: RegExp -> RegExp -> RegExp
(<**>) e1 e2 =
  \s -> or [ e1 prefix && e2 suffix | (prefix,suffix) <- drop 1 (splits s)]

star :: RegExp -> RegExp
star e = epsilon ||| (e <**> star e)

-- put your solutions here

-- option

option :: RegExp -> RegExp
option e = epsilon ||| e 

-- plus
plus :: RegExp -> RegExp
plus e = e <**> star e


-- range 
range :: Char -> Char -> RegExp
range c1 c2 = foldr1 (|||) [ char x | x <- [c1..c2] ]
	

-- number
number :: RegExp
number e = ((range '0' '9') ||| (range '1' '9' <**> plus (range '0' '9'))) e

-- fractional number
fractional :: RegExp
-- the special case for a single 0 after decimal ruined the elegance
--fractional e = (number <**> (char '.') <**> (((plus (range '0' '9')) <**> (range '1' '9')) ||| (range '0' '9'))) e
fractional e = (number <**> (char '.') <**> (((plus(range '0' '9')) <**> (plus(range '1' '9')) ||| (range '0' '9'))))e
