module SimpleFunctions where


--a)
filterFirst :: (a -> Bool) -> [a] -> [a]
--take care of empty set case, we don't care what the property is here
filterFirst _ [] = []
filterFirst p (x:xs) =
    --if the first element in the list satisfies the property p then return the first element of the list
    --and run filterFirst on the rest of the list
    if ( (p x) == True)
        then x:filterFirst p xs
        --if the first element in the list does not satisfy the property p then return the rest of the list
        else if ( (p x) == False)
                then xs
                --else return the first element and continue running filterFirst on the rest of the list
                else (x:filterFirst p xs)
    

--so that filterFirst p xs removes the first element of xs which does not
--satisfy the property p. For instance,

--  filterFirst even [1,2,3,4] ~~> [2,3,4]

  
--b)
filterLast :: (a -> Bool) -> [a] -> [a]
--take care of empty set case, we don't care what the property is here
filterLast _ [] = []
filterLast p x = []
    --if the last element in the list satisfies the property p then return the first element of the list
    --and run filterLast on the rest of the list
--    if( (p (last x)) == True)
--        then (last x):filterLast p x
        --if the last element in the list does not satisfy the property p then return the rest of the list
--        else if ( (p (last x)) == False)
--            then reverse(tail(reverse x))
            --else return the first element and continue running filterLast on the rest of the list
--            else (last x):filterLast p (tail x)


--so that filterLast p xs removes the last element of xs which does not
--satisfy the property p. For instance,

--  filterLast even [1,2,3,4] ~~> [1,2,4]

--c)
split :: [a] -> ([a],[a])
--take care of empty set case
split [] = ([],[])
--take care of single element case
split [x] = ([x],[])
--take care of general case using pattern matching
--split list into x:y:xs pattern in order to manipulate different
--parts of the list freely. Recursively split xx and ys (parts of the list)
--into tuples until there is nothing left to split
split (x:y:xs) = (x:xx, y:ys) 
    where (xx, ys) = split xs

--so that split will split a list into two lists, picking elements 
--alternately.  For instance,

--  split [1,2,3,0,4] ~~> ([1,3,4],[2,0])

--d)  
interleave :: ([a],[a]) -> [a]
--take care of double empty list case
interleave ([],[]) = []
--take care of singleton list cases and single list cases
interleave ([x],[]) = [x]
interleave ([],[x]) = [x]
interleave (xs,[]) = xs
interleave ([],ys) = ys
--use pattern matching to break each list into "head" and "rest" components
--combine the 1st 2 elements of each list then recursively call interleave on the "rest"
interleave (x:xs,y:ys) = [x]++[y]++interleave(xs,ys)

--so that interleave will interleave two lists. For instance,

--  interleave ([1,3,4],[2,0]) ~~> [1,2,3,0,4]

--e)  
merge :: (Ord a) => ([a],[a]) -> [a]
--take care of double empty list case
merge ([],[]) = []
--take care of both singleton list cases
merge (ls, []) = ls
merge ([], rs) = rs
--simply compare the heads of each list, if the 1st condition is true
--then take the 1st list's head and call merge on the rest
--otherwise take the 2nd list's head and call merge on the rest
merge (l:ls,r:rs)
    | l <= r = l:merge(ls, (r:rs))
    | otherwise = r:merge((l:ls), rs)

--so that merge merges two sorted lists to produce a sorted list. 
--For instance,

--  merge ([1,2,3],[4,5]) = [1,2,3,4,5]

--f)  
mergeSort :: (Ord a) => [a] -> [a]
--take care of empty list case
mergeSort [] = []
--if list length is too small to split then return list
--else merge the two "halves" (recursively splits each list until base case is met)
--store each half into j and k respectively. Then store the recursive calls to mergeSort in 
--fstHalf and sndHalf to be merged in the general case
mergeSort x = if (length x < 2)
                 then x
                 else merge(fstHalf, sndHalf)
               where
                     (j,k) = split x
                     fstHalf = mergeSort(j)
                     sndHalf = mergeSort(k)


--so that it realizes the mergesort algorithm. You have to use your 
--functions split (c) and merge (e).

--referenced sources: http://en.literateprograms.org/Merge_sort_(Haskell)
--                    https://en.wikibooks.org/wiki/Haskell/Pattern_matching


