mymap :: (a -> b) -> [a] -> [b]
mymap _ [] = []
mymap f (x:xs) = f x : mymap f xs

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter f (x:xs) 
	| f x = x : myfilter f xs
	| otherwise = myfilter f xs

filterLast :: (a -> Bool) -> [a] -> [a]
filterLast _ [] = []
filterLast p x 
	| not (p (last x)) = init x
	| otherwise = filterLast p (init x) ++ [last x]


data Tree a = Nil | Node a (Tree a) (Tree a)
freeTree =   
    Node 'P'  
        (Node 'O'  
            (Node 'L'  
                (Node 'N' Nil Nil)  
                (Node 'T' Nil Nil)  
            )  
            (Node 'Y'  
                (Node 'S' Nil Nil)  
                (Node 'A' Nil Nil)  
            )  
        )  
        (Node 'L'  
            (Node 'W'  
                (Node 'C' Nil Nil)  
                (Node 'R' Nil Nil)  
            )  
            (Node 'A'  
                (Node 'A' Nil Nil)  
                (Node 'C' Nil Nil)  
            )  
        )  
traverse :: Tree a -> [a]
traverse Nil = []
traverse (Node n left right) = n : traverse left ++ traverse right


concat1 :: [a] -> [a]
concat1 [] = []
concat1 [x] = [x]
concat1 (x:xs) = [x] ++ concat1 xs

occurs :: (Eq a) => Tree a -> a -> Bool
occurs Nil _ = False
occurs (Node n left right) e
	| n == e = True
	| otherwise = (occurs left e) || (occurs right e)

data Shapes = Rectangle Float Float | Circle Float

area :: Shapes -> Float
area (Rectangle w h) = w * h
area (Circle r) = 3.14 * r ** 2

tail1 :: [a] -> [a]
tail1 [] = []
tail1 x = reverse(init(reverse x))

bigger :: Int -> Int -> Int 
bigger a b  
	| a > b = a
	| otherwise = b

merge :: Ord a => [a] -> [a] -> [a]
merge [] [] = []
merge x [] = x
merge [] y = y
merge (x:xs) (y:ys)
	| x <= y = x:merge (xs) (y:ys)
	| otherwise = y:merge (x:xs) (ys)
