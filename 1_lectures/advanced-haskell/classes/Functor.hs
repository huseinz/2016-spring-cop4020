-- example: tree is a functor

data Tree a = Nil | Node a (Tree a) (Tree a) deriving (Show,Eq)

-- documentation
-- https://hackage.haskell.org/package/base-4.8.2.0/docs/Data-Functor.html
-- source code
-- https://hackage.haskell.org/package/base-4.8.2.0/docs/src/GHC.Base.html#Functor

instance Functor Tree where

  -- fmap :: (a -> b) -> f a -> f b
  
  fmap _ Nil                 = Nil
  fmap f (Node x left right) = Node (f x) (fmap f left) (fmap f right)

    
t :: Tree Int
t = Node 10 (Node 5 Nil Nil) (Node 20 Nil Nil)

-- show the following examples in class
-- Maybe and []

