import Control.Applicative

-- documentation
-- https://hackage.haskell.org/package/base-4.8.2.0/docs/Control-Applicative.html

-- source code
-- https://hackage.haskell.org/package/base-4.8.2.0/docs/src/GHC.Base.html#Applicative

-- section on applicative functors in Learn you a Haskell
-- http://learnyouahaskell.com/functors-applicative-functors-and-monoids#applicative-functors

-- show examples in class
-- Maybe and []


