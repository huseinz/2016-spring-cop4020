import Control.Monad.State

newtype Stack a = Stack [a] deriving (Show,Eq)

pop :: State (Stack a) a
pop = do
  (Stack (x:xs)) <- get
  put (Stack xs)
  return x

push :: a -> State (Stack a) ()
push x = do
  (Stack xs) <- get
  put (Stack (x:xs))

stackManip :: State (Stack Int) ()
stackManip = do
  a <- pop
  if even a 
    then
      let b = 2 * a in
      push b           -- no do necessary because only one action
    else
      let c = 3 * a in 
      do               -- do necessary because c occurs 
        push c         -- in first action and
        push c         -- in second action

stackManip2 :: State (Stack Int) ()
stackManip2 = do
  a <- pop
  b <- pop
  push (a+b)


test = (runState stackManip) (Stack [1,2,3])
test2 = (runState stackManip2) (Stack [10,2,3])


