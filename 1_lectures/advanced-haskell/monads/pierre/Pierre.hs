--       
--     \ *
--      \|
--       |\#
--       | \#
--      / \
--     /   \
----------------------

type Birds = Int
type Pole = (Birds, Birds)

landLeft :: Birds -> Pole -> Maybe Pole
landLeft n (left, right) =
  if abs (left + n - right) > 3
     then Nothing
     else Just (left + n, right)

landRight :: Birds -> Pole -> Maybe Pole
landRight n (left, right) =
  if abs (left - (right + n)) > 3
     then Nothing
     else Just (left, right + n)

banana :: Pole -> Maybe Pole
banana _ = Nothing




