-- my own implementation of the stateT and MonadTrans monads

module RPNCalc where

import Control.Monad.Trans.Class

newtype StateT s m a = StateT { runStateT :: s -> m (a, s) }

instance (Monad m) => Monad (StateT s m) where
  return x = StateT $ \s -> return (x, s)

  (StateT h) >>= f = StateT $ \s -> do
                       (a, s') <- h s
                       runStateT (f a) s'

instance MonadTrans (StateT s) where
  lift c = StateT $ \s -> c >>= (\x -> return (x,s))

-- state and io combined

newtype Stack = Stack [Float] deriving (Show,Eq)

pop :: StateT (Stack) IO Float
pop = StateT $ \(Stack (x:xs)) -> return (x, Stack xs)

push :: Float -> StateT Stack IO ()
push x = StateT $ \(Stack xs) -> return ((), Stack (x:xs))

isOp :: String -> Bool
isOp inp = elem inp ["+","-","*","/"]

getOp :: String -> (Float -> Float -> Float)
getOp "+" = (+)
getOp "-" = (-)
getOp "*" = (*)
getOp "/" = (/)

-- reverse Polish notation calculator

rpnCalc :: StateT Stack IO ()
rpnCalc = do
  inp <- lift getLine
  if isOp inp
    then do
      b <- pop
      a <- pop
      let r = (getOp inp) a b
      lift $ putStrLn (show r)
      push r
    else push ((read inp)::Float)
  rpnCalc

run = runStateT rpnCalc (Stack [])
