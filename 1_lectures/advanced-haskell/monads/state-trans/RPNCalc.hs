module RPNCalc where

import Control.Monad.State.Lazy

-- state and io combined

newtype Stack = Stack [Float] deriving (Show,Eq)

pop :: StateT Stack IO Float
--pop = StateT $ \(Stack (x:xs)) -> return (x, Stack xs)
pop = do
  (Stack (x:xs)) <- get
  put (Stack xs)
  return x

push :: Float -> StateT Stack IO ()
--push x = StateT $ \(Stack xs) -> return ((), Stack (x:xs))
push x = do
  (Stack xs) <- get
  put (Stack (x:xs))

isOp :: String -> Bool
isOp inp = elem inp ["+","-","*","/"]

getOp :: String -> (Float -> Float -> Float)
getOp "+" = (+)
getOp "-" = (-)
getOp "*" = (*)
getOp "/" = (/)

-- reverse Polish notation calculator

rpnCalc :: StateT Stack IO ()
rpnCalc = do
  inp <- lift getLine
  if isOp inp
    then do
      b <- pop
      a <- pop
      let r = (getOp inp) a b
      lift $ putStrLn (show r)
      push r
    else push ((read inp)::Float)
  rpnCalc

run = runStateT rpnCalc (Stack [])
