-- my own implementation of the state monad

module State where

--      Operation
--      Instruction
newtype State s a = State { runState :: s -> (a, s) }

instance Monad (State s) where
  return x = State $ \s -> (x, s)

  (State h) >>= f = State $ \s ->
                      let (a, s') = h s
                      in runState (f a) s'
                  
