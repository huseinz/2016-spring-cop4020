import Data.List

-- first variant

perm :: Eq a => [a] => [[a]]

perm [] = [[]]
perm xs = [ x:ps | x <- xs, ps <- perm (xs\\[x]) ]



-- second variant

perm' :: [a] -> [[a]]

perm' []     = [[]]
perm' (x:xs) = [ ps ++ [x] ++ qs | rs <- perm' xs ,
                                   (ps,qs) <- splits rs ]

splits :: [a] -> [([a],[a])]

splits []     = [ ([],[]) ]
splits (y:ys) = ([],y:ys) : [ (y:ps,qs) | (ps,qs) <- splits ys]

