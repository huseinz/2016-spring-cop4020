-- lazy implementation of the Sieve of Eratosthenes
-- see https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes

primes :: [Integer]

primes = sieve [2..]

sieve :: [Integer] -> [Integer]
sieve (x:xs) = x : sieve [y | y <- xs, y `mod` x > 0]
