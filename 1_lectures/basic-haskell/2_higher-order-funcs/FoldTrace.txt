See https://wiki.haskell.org/Fold

================

   RIGHT FOLD

================

maximum' :: (Ord a) => [a] -> a
maximum' []     = error "empty list"
maximum' (x:xs) = foldr max x xs

Trace for input list [1,2,10,3]
pattern matching yields x = 1, xs = [2,10,3]
 x is the starting value for foldr
xs is the input list for foldr  

      :
     / \
    2   :
       / \
      10  :
         / \
        3  []

a) Subst [] by starting value 1

~~>
      :
     / \
    2   :
       / \
      10  :
         / \
        3   1

b) Subst the rightmost : with max

~~>
      :
     / \
    2   :
       / \
      10 max
         / \
        3   1

c) Eval max using the args 3 and 1 below and replace max by outcome

~~>
      :
     / \
    2   :
       / \
      10  3
        
repeat steps b), c)

================

   LEFT FOLD

================

maximum' :: (Ord a) => [a] -> a
maximum' []     = error "empty list"
maximum' (x:xs) = foldl max x xs

Trace for input list [1,2,10,3]
pattern matching x = 1, xs = [2,10,3]  

2:10:3:[]

a) Prepend starting value 1, drop [] at the end

~~> 1:2:10:3

b) Subst the leftmost : with max

~~> 1 max 2:10:3

c) Eval max using (adjacent) args 1 and 2 and replace max by outcome

~~> 2:10:3

