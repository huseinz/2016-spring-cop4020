quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =
  quicksort left ++ [x] ++ quicksort right
  where
    left =  filter (\y -> y < x) xs
    right = [y | y <- xs, y >= x]
