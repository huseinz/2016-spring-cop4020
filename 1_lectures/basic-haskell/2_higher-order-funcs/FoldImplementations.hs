-- See
-- https://wiki.haskell.org/Fold
-- http://learnyouahaskell.com/higher-order-functions#folds

foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' _ s []     = s
foldr' f s (x:xs) = f x (foldr f s xs)

foldl' :: (a -> b -> a) -> a -> [b] -> a
foldl' _ s []     = s
foldl' f s (x:xs) = foldl f (f s x) xs
