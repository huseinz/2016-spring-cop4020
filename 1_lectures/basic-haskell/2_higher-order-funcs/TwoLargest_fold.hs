module TwoLargestInClass where

getStartingValue :: (Ord a) => a -> a -> (a,a)
getStartingValue x0 x1 = 
  if x0 >= x1 
    then (x0,x1)
    else (x1,x0) 

getTwoLargest :: (Ord a) => [a] -> (a,a)

-- explaining pattern matching
getTwoLargest []         = error "empty list"
getTwoLargest [_]        = error "list contains only one element"
getTwoLargest (y0:y1:ys) = foldr update (getStartingValue y0 y1) ys

--                   input 1
--                          input 2
--                                 output   
update :: (Ord a) => a -> (a,a) -> (a,a)
update y (x0,x1) =
  if y >= x0
    then (y,x0)
    else if y >= x1
      then (x0,y)
      else (x0,x1)
