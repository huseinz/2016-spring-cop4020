module FoldDisplay where

import Fold

displayRightFold :: (Show a) => [a] -> a -> String 
displayRightFold zs s =
  foldr' (\x y -> concat ["(f ",x," ",y,")"]) (show s) (map show zs)

displayLeftFold :: (Show a) => [a] -> a -> String 
displayLeftFold zs s =
  foldl' (\x y -> concat ["(f ",x," ",y,")"]) (show s) (map show zs)
