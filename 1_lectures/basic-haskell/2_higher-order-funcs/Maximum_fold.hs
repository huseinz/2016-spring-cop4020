-- Reimplement the built-in function maximum with fold 

maximum' :: (Ord a) => [a] -> a
maximum' [] = error "empty list"
maximum' (x:xs) =
  foldr max x xs

maximum'' :: (Ord a) => [a] -> a
maximum'' [] = error "empty list"
maximum'' (x:xs) =
  foldl max x xs

  


