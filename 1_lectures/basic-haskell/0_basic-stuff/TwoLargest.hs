module TwoLargestInClass where

getTwoLargest :: (Ord a) => [a] -> (a,a)

-- explaining pattern matching
getTwoLargest []         = error "empty list"
getTwoLargest [_]        = error "list contains only one element"

-- base case
getTwoLargest [x0,x1] = 
  if x0 >= x1 
    then (x0,x1)
    else (x1,x0)

-- inductive case
getTwoLargest (y:ys) =
  update (getTwoLargest ys) y

--                   input 1
--                          input 2
--                                 output   
update :: (Ord a) => (a,a) -> a -> (a,a)
update (x0,x1) y =
  if y >= x0
    then (y,x0)
    else if y >= x1
      then (x0,y)
      else (x0,x1)
