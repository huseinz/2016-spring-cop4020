
module Hindex where

import Data.List -- sort is defined in this module

hIndex :: [Int] -> Int
hIndex [] = 0
hIndex xs = 
  hIndex_iter (reverse (sort xs)) 1

hIndex_iter :: [Int] -> Int -> Int

hIndex_iter []     r = r - 1

hIndex_iter (x:xs) c = 
  if x >= c
    then hIndex_iter xs (c+1)
    else (c-1)


 