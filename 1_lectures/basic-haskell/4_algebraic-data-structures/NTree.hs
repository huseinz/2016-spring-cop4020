data NTree = Nil | Node Integer NTree NTree

makeNode :: Integer -> NTree
makeNode n = (Node n Nil Nil)

sumNTree :: NTree -> Integer
sumNTree Nil = 0
sumNTree (Node n left right) = n + sumNTree left + sumNTree right

depthNTree :: NTree -> Integer
depthNTree Nil = 0
depthNTree (Node _ left right) = 1 + max (depthNTree left) (depthNTree right)

occurs :: NTree -> Integer -> Integer
occurs Nil _ = 0
occurs (Node n left right) m
  | n == m    = 1 + occurs left m + occurs right m 
  | otherwise =     occurs left m + occurs right m

traverseDepthFirst :: NTree -> [Integer]
traverseDepthFirst Nil = []
traverseDepthFirst (Node n left right) =
  n:(traverseDepthFirst left ++ traverseDepthFirst right)

traverseBreadthFirst :: NTree -> [Integer]
traverseBreadthFirst t = traverseBreadthFirst_iter [t] []

--  [(Node 2 Nil Nil)] []
--  [Nil,Nil]          [2]
--  [Nil]              [2]
--  []                 [2]

traverseBreadthFirst_iter :: [NTree] -> [Integer] -> [Integer]
traverseBreadthFirst_iter [] result = result
traverseBreadthFirst_iter (t:ts) partial = 
  case t of
    Nil                 -> traverseBreadthFirst_iter ts partial
    (Node n left right) ->
      traverseBreadthFirst_iter (ts ++ [left,right]) (partial ++ [n]) 
