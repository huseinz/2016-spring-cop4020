% L1
% L2
% L3

% what does it mean that L3 is the 
% concat of L1 and L2

concat_lists([], L, L).

concat_lists([H|T1], L2, [H|T3]) :- concat_lists(T1, L2, T3).  
