naive_rev([],[]).

naive_rev([H|T], R) :- 
  naive_rev(T, RevT), append(RevT,[H],R).

% naive_rev :: [a] -> [a]
% naive_rev [] = []
% naive_rev (x:xs) = reverse xs ++ [x]



