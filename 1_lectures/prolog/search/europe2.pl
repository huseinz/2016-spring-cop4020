borders1( belgium, france ).
borders1( france, spain ).
borders1( germany, austria ).
borders1( germany, denmark ).
borders1( germany, switzerland ).
borders1( germany, france ).
borders1( switzerland, france ).
borders1( netherlands, belgium ).
borders1( netherlands, germany ).
borders1( spain, portugal ).

borders( X, Y ) :- borders1( X, Y ).
borders( X, Y ) :- borders1( Y, X ).

% warning this algorithm ends up in an infinite loop in some cases
% for instance,
% route( denmark, portugal, X ) leans to an infinite loop

route( A, B, [drive(A, B)]) :-
  borders( A, B ).

route( A, B, [drive(A,Z)|ZtoB] ) :-
  borders( A, Z ),
  route( Z, B, ZtoB ).
