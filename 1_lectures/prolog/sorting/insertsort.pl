insert(X, [], [X]).

insert(X, [H | T], [X, H | T]) :-
  X =< H.

insert(X, [H | T1], [H | T2]) :-
  X > H,
  insert(X, T1, T2).

insertSort([], []).

insertSort([H | T1], L) :-
  insertSort(T1, L1),
  insert(H, L1, L).
