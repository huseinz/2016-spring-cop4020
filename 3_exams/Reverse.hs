import Prelude hiding (reverse)

-- reimplement reverse using primitive
-- recursion

reverse :: [a] -> [a]
reverse []     = []
reverse (x:xs) = (reverse xs) ++ [x]

-- what is bad about this implementation?
-- do it better

reverse' :: [a] -> [a]
reverse' xs = reverse_iter xs []

reverse_iter :: [a] -> [a] -> [a]
reverse_iter []     ys = ys
reverse_iter (x:xs) ys =
  reverse_iter xs (x:ys)



