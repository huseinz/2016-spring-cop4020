-- makeup exam

-- a

data Shape = Rectangle Double Double | Circle Double

circumference :: Shape -> Double
circumference (Rectangle w h) = 2 * (w + h)
circumference (Circle r)      = 2 * pi * r

-- b

data Tree a = Nil | Node a (Tree a) (Tree a)
                deriving (Show,Eq)

replace :: Eq a => a -> a -> Tree a -> Tree a
replace _ _ Nil = Nil
replace x y (Node z left right) 
  | (z == x)  = Node y left' right'
  | otherwise = Node x left' right'
    where
      left'  = (replace x y left)
      right' = (replace x y right)

-- c

occursNum :: Eq a => Tree a -> a -> Int
occursNum Nil _ = 0
occursNum (Node x left right) y =
  (if (x == y) then 1 else 0) + (occursNum left y) + (occursNum right y)
