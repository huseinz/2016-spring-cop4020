-- suffixes
-- suffixes [1,2,3] ~~>
-- [[1,2,3],[2,3],[3],[]]

suffixes :: [a] -> [[a]]
suffixes [] = [[]]
suffixes xs = xs:(suffixes (tail xs))    

preffixes :: [a] -> [[a]]
preffixes xs =
  map reverse (suffixes (reverse xs))

-- exercise:
-- reimplement take, drop, (!!)

